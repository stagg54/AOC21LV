﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Explode.vi" Type="VI" URL="../Explode.vi"/>
		<Item Name="Part 1.vi" Type="VI" URL="../Part 1.vi"/>
		<Item Name="Part 2.vi" Type="VI" URL="../Part 2.vi"/>
		<Item Name="Snail Add.vi" Type="VI" URL="../Snail Add.vi"/>
		<Item Name="Snail Magnitude.vi" Type="VI" URL="../Snail Magnitude.vi"/>
		<Item Name="Snail Reduce.vi" Type="VI" URL="../Snail Reduce.vi"/>
		<Item Name="Snail Split.vi" Type="VI" URL="../Snail Split.vi"/>
		<Item Name="Stack.lvclass" Type="LVClass" URL="../Stack/Stack.lvclass"/>
		<Item Name="Test.lvclass" Type="LVClass" URL="../Test/Test.lvclass"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="lib_GUnit.lvlib" Type="Library" URL="/&lt;vilib&gt;/GUnit/GUnit/lib_GUnit.lvlib"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
